import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import { InputGroup, InputGroupAddon, Input } from "reactstrap";
import Card from "react-bootstrap/Card";
import CardColumns from "react-bootstrap/CardColumns";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useState } from "react";
import bottle from "./assets/bottle.jpg";
import bag from "./assets/bag.jpg";
import drug from "./assets/drug.jpg";
import food from "./assets/food.jpg";
import it from "./assets/Plain-pvc.png";
import other from "./assets/other.jpg";
import Amplify, { API } from "aws-amplify";
// import ControlledCarousel from "./components/Carouselhome.js";

function Submit() {
  alert("SUCCESS");
}

function Recycle() {
  const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);
  const [count3, setCount3] = useState(0);
  const [count4, setCount4] = useState(0);
  const [count5, setCount5] = useState(0);
  const [count6, setCount6] = useState(0);

  async function submitPlastic() {
    const apiName = "GreenmeAPI";
    const path = "/plastic/plastic-transaction";
    const myInit = {
      headers: { "Content-Type" : "application/json"},
      body: {
        "Username": "punpun",
        "id_transction": 23456,
        "quantity": (count1 + count2 + count3 + count4 + count5 + count6),
      },
    };
    try {
      return await API.post(apiName, path, myInit);
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="Recycle container-fluid">
      <Container className="ranktable">
        <Row>
          <Col>
            <CardColumns>
              <Card
                style={{
                  width: "300px",
                  marginTop: "20px",

                  borderRadius: "10px",
                }}
              >
                <Card.Img variant="top" src={bottle} height="250px" />
                <Card.Body height="300px">
                  <Card.Title className="title-menu">Plastic bottle</Card.Title>
                  <Card.Text>ขวดพลาสติก, ขวดน้ำดื่ม, ขวดน้ำมันพืช</Card.Text>
                </Card.Body>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <button
                      variant="secondary"
                      onClick={() => setCount1(count1 - 1)}
                      style={{
                        width: "25px",
                        background: "#DCDCDC",
                        border: "#fde0e0",
                      }}
                    >
                      -
                    </button>
                  </InputGroupAddon>
                  <Input className="text-center" placeholder={count1} />
                  <InputGroupAddon addonType="append">
                    <button
                      variant="outline-primary"
                      onClick={() => setCount1(count1 + 1)}
                      style={{
                        width: "25px",
                        background: "#bbded6",
                        border: "#fde0e0",
                      }}
                    >
                      +
                    </button>
                  </InputGroupAddon>
                </InputGroup>
              </Card>
              {/* </Col>

          <Col> */}
              <Card
                style={{
                  width: "300px",
                  marginLeft: "200px",
                  marginTop: "20px",
                  borderRadius: "10px",
                }}
              >
                <Card.Img variant="top" src={bag} height="250px" />
                <Card.Body height="300px">
                  <Card.Title className="title-menu">Plastic bag</Card.Title>
                  <Card.Text>ถุงพลาสติก, ถุงใส่ของ</Card.Text>
                </Card.Body>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <button
                      variant="secondary"
                      onClick={() => setCount2(count2 - 1)}
                      style={{
                        width: "25px",
                        background: "#DCDCDC",
                        border: "#fde0e0",
                      }}
                    >
                      -
                    </button>
                  </InputGroupAddon>
                  <Input className="text-center" placeholder={count2} />
                  <InputGroupAddon addonType="append">
                    <button
                      variant="outline-primary"
                      onClick={() => setCount2(count2 + 1)}
                      style={{
                        width: "25px",
                        background: "#bbded6",
                        border: "#fde0e0",
                      }}
                    >
                      +
                    </button>
                  </InputGroupAddon>
                </InputGroup>
              </Card>
              {/* </Col> */}
              {/* <Col> */}
              <Card
                style={{
                  width: "300px",
                  marginLeft: "400px",
                  marginTop: "20px",
                  borderRadius: "10px",
                }}
              >
                <Card.Img variant="top" src={food} height="250px" />
                <Card.Body height="300px">
                  <Card.Title className="title-menu">Food container</Card.Title>
                  <Card.Text>ถ้วย, จาน, ชาม พลาสติก</Card.Text>
                </Card.Body>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <button
                      variant="secondary"
                      onClick={() => setCount3(count3 - 1)}
                      style={{
                        width: "25px",
                        background: "#DCDCDC",
                        border: "#fde0e0",
                      }}
                    >
                      -
                    </button>
                  </InputGroupAddon>
                  <Input className="text-center" placeholder={count3} />
                  <InputGroupAddon addonType="append">
                    <button
                      variant="outline-primary"
                      onClick={() => setCount3(count3 + 1)}
                      style={{
                        width: "25px",
                        background: "#bbded6",
                        border: "#fde0e0",
                      }}
                    >
                      +
                    </button>
                  </InputGroupAddon>
                </InputGroup>
              </Card>
            </CardColumns>
          </Col>
          <Col></Col>
        </Row>

        <Row>
          <Col>
            <CardColumns>
              <Card
                style={{
                  width: "300px",
                  marginTop: "20px",

                  borderRadius: "10px",
                }}
              >
                <Card.Img variant="top" src={drug} height="250px" />
                <Card.Body height="300px">
                  <Card.Title className="title-menu">drug packaging</Card.Title>
                  <Card.Text>ฝาขวด , ขวดยา</Card.Text>
                </Card.Body>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <button
                      variant="secondary"
                      onClick={() => setCount4(count4 - 1)}
                      style={{
                        width: "25px",
                        background: "#DCDCDC",
                        border: "#fde0e0",
                      }}
                    >
                      -
                    </button>
                  </InputGroupAddon>
                  <Input className="text-center" placeholder={count4} />
                  <InputGroupAddon addonType="append">
                    <button
                      variant="outline-primary"
                      onClick={() => setCount4(count4 + 1)}
                      style={{
                        width: "25px",
                        background: "#bbded6",
                        border: "#fde0e0",
                      }}
                    >
                      +
                    </button>
                  </InputGroupAddon>
                </InputGroup>
              </Card>

              <Card
                style={{
                  width: "300px",
                  marginLeft: "200px",
                  marginTop: "20px",
                  borderRadius: "10px",
                }}
              >
                <Card.Img variant="top" src={it} height="250px" />
                <Card.Body height="300px">
                  <Card.Title className="title-menu">IT</Card.Title>
                  <Card.Text>
                    ท่อน้ำประปา และ อุปกรณ์ไฟฟ้าและอิเล็กทรอนิกส์ อาทิเช่น
                    ฉนวนหุ้มสายไฟ
                  </Card.Text>
                </Card.Body>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <button
                      variant="secondary"
                      onClick={() => setCount5(count5 - 1)}
                      style={{
                        width: "25px",
                        background: "#DCDCDC",
                        border: "#fde0e0",
                      }}
                    >
                      -
                    </button>
                  </InputGroupAddon>
                  <Input className="text-center" placeholder={count5} />
                  <InputGroupAddon addonType="append">
                    <button
                      variant="outline-primary"
                      onClick={() => setCount5(count5 + 1)}
                      style={{
                        width: "25px",
                        background: "#bbded6",
                        border: "#fde0e0",
                      }}
                    >
                      +
                    </button>
                  </InputGroupAddon>
                </InputGroup>
              </Card>

              <Card
                style={{
                  width: "300px",

                  marginLeft: "400px",
                  marginTop: "20px",
                  borderRadius: "10px",
                }}
              >
                <Card.Img variant="top" src={other} height="250px" />
                <Card.Body height="300px">
                  <Card.Title className="title-menu">Other</Card.Title>
                  <Card.Text>ปากกา, ขวดนมเด็ก, หมวกนิรภัย</Card.Text>
                </Card.Body>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <button
                      variant="secondary"
                      onClick={() => setCount6(count6 - 1)}
                      style={{
                        width: "25px",
                        background: "#DCDCDC",
                        border: "#fde0e0",
                      }}
                    >
                      -
                    </button>
                  </InputGroupAddon>
                  <Input className="text-center" placeholder={count6} />
                  <InputGroupAddon addonType="append">
                    <button
                      variant="outline-primary"
                      onClick={() => setCount6(count6 + 1)}
                      style={{
                        width: "25px",
                        background: "#bbded6",
                        border: "#fde0e0",
                      }}
                    >
                      +
                    </button>
                  </InputGroupAddon>
                </InputGroup>
              </Card>
            </CardColumns>
          </Col>
          <Col></Col>
        </Row>

        <Button
          onClick={() => {
            Submit();
            submitPlastic();
            setCount1(0);
            setCount2(0);
            setCount3(0);
            setCount4(0);
            setCount5(0);
            setCount6(0);
          }}
          style={{
            width: "100px",
            background: "#34691c",
            border: "#34691c",
            marginTop: "30px",
            marginBottom: "30px",
          }}
        >
          SUBMIT
        </Button>
      </Container>
    </div>
  );
}

export default Recycle;
