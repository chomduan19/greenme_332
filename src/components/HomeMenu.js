import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import m1 from "./img/menu1.png";
import m2 from "./img/plant3.jpg";
import m3 from "./img/reward.jpg";
import m4 from "./img/team.jpg";
import { NavLink } from "react-router-dom";
import Container from "react-bootstrap/Container";
function HomeMenu() {
  return (
    <Container>
      <Row>
        <Col sm="true">
          <Card
            style={{
              width: "200px",
              marginTop: "20px",

              borderRadius: "10px",
            }}
          >
            <Card.Img variant="top" src={m1} height="150px" />
            <Card.Body height="300px">
              <Card.Title className="title-menu">Recycle</Card.Title>
              <Card.Text>
                Sending your plastic waste to us and receiving the reward!
              </Card.Text>
              <Button style={{ background: "#326125", border: "#326125" }}>
                <NavLink
                  className="nav-link "
                  to="/recycle"
                  style={{ color: "#FFFFFF" }}
                >
                  Recycle
                </NavLink>
              </Button>
            </Card.Body>
          </Card>
        </Col>

        <Col sm="true">
          <Card
            style={{
              width: "200px",
              marginLeft: "15px",
              marginTop: "20px",
              borderRadius: "10px",
            }}
          >
            <Card.Img variant="top" src={m3} height="150px" />
            <Card.Body height="300px">
              <Card.Title className="title-menu">Reward</Card.Title>
              <Card.Text>
                Exchange your O2 to your lovely gifts ,meals or more
              </Card.Text>
              <Button style={{ background: "#326125", border: "#326125" }}>
                <NavLink
                  className="nav-link "
                  to="/reward"
                  style={{ color: "#FFFFFF" }}
                >
                  Get Reward
                </NavLink>
              </Button>
            </Card.Body>
          </Card>
        </Col>
        <Col sm="true">
          <Card
            style={{
              width: "200px",
              marginLeft: "15px",
              marginTop: "20px",
              borderRadius: "10px",
            }}
          >
            <Card.Img variant="top" src={m2} height="150px" />
            <Card.Body height="300px">
              <Card.Title className="title-menu">Plant</Card.Title>
              <Card.Text>
                Plant your tree and you will get a big surprise.
              </Card.Text>
              <Button style={{ background: "#326125", border: "#326125" }}>
                <NavLink
                  className="nav-link "
                  to="/plant"
                  style={{ color: "#FFFFFF" }}
                >
                  Plant
                </NavLink>
              </Button>
            </Card.Body>
          </Card>
        </Col>
        <Col sm="true">
          <Card
            style={{ width: "200px", marginTop: "20px", borderRadius: "10px" }}
          >
            <Card.Img variant="top" src={m4} height="150px" />
            <Card.Body height="300px">
              <Card.Title className="title-menu">Rank</Card.Title>
              <Card.Text>
                See your ranking in your community and your teammate
              </Card.Text>
              <Button style={{ background: "#326125", border: "#326125" }}>
                <NavLink
                  className="nav-link "
                  to="/ranking"
                  style={{ color: "#FFFFFF" }}
                >
                  Ranking
                </NavLink>
              </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default HomeMenu;
