import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import logo from './img/GREENME.png';
import {NavLink} from 'react-router-dom';
// import Avatar from '@material-ui/core/Avatar';
// import {ThemeProvider} from '@material-ui/styles';
import {AmplifySignOut } from "@aws-amplify/ui-react";




function Navhead(){
    return(
        <div>
        <Navbar className="navbar" bg="white" variant="light" fixed="top" margin ="0" padding="50">
        <div className="container nav__text" >
        <Navbar.Brand href="#home" ><img
            alt=""
            src={logo}
            width="80"
            height="40"
            
        />{' '}
        </Navbar.Brand>
        <Nav className="mr-auto" float="right">
            <Nav.Link ><NavLink className ="nav-link" exact activeClassName="active" to="/" activeStyle={{ color: '#3F6845' ,textDecoration :' none'}} >Home</NavLink></Nav.Link>
            <Nav.Link ><NavLink className ="nav-link" activeClassName="active" to="/plant" activeStyle={{ color: '#3F6845' ,textDecoration :' none' }}>Plant</NavLink></Nav.Link>
            <Nav.Link ><NavLink className ="nav-link" activeClassName="active" to="/recycle" activeStyle={{ color: '#3F6845' ,textDecoration :' none' }}>Recycle</NavLink></Nav.Link>
            <Nav.Link ><NavLink className ="nav-link" activeClassName="active" to="/reward" activeStyle={{ color: '#3F6845' ,textDecoration :' none' }}>Reward</NavLink></Nav.Link>
            <Nav.Link ><NavLink className ="nav-link" activeClassName="active" to="/ranking" activeStyle={{ color: '#3F6845' ,textDecoration :' none' }}>Ranking</NavLink></Nav.Link>
            
        </Nav>
        <div style={{ display: "flex" }}>
        {/* <Avatar classes={ThemeProvider}>P</Avatar> */}
            <AmplifySignOut style={{ marginRight: "auto",marginLeft:"30px", background: "#74B72E" ,border: "#74B72E" }}/>
               
        </div>
        </div>
  </Navbar>
  </div>
    );
}

export default Navhead;