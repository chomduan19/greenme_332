import React from "react";
import Card from "react-bootstrap/Card";
// import CardColumns from "react-bootstrap/CardColumns";
// import Row from "react-bootstrap/Row";
// import Col from "react-bootstrap/Col";
import s1 from "./img/Rank1.png";
import s2 from "./img/Rank2.png";
import s3 from "./img/Rank3.png";
import champ from "./img/Cham.png";
import Container from "react-bootstrap/Container";

function MiniRank() {
  return (

    <div className="card md-2" style={{ width: "300px" ,marginTop:"20px" ,borderRadius:"10px", backgroundColor :"#C8D5B9", border:"#FFFFFF"}}>
      <div style={{ marginLeft:"20px" ,marginTop:"20px" }}>
      <img alt="" src = {champ} width="100" />
      <Card.Title  className="title-menu" style={{ textAlign: "center" ,marginTop:"20px" }}>Weekly Top3</Card.Title>
      </div>
      <Container>
      <div className="row no-gutters" style={{marginTop:"5px",marginBottom:"5px",backgroundColor :"#FFFFFF" ,borderRadius:"10px"}}>
        <div className="col-md-4 " style={{marginTop:"20px" }}>
          <img
            className="bd-placeholder-img"
            width="50"
            height="50"
            src ={s1}
            
            alt=""
          />
        
        </div>
        <div className="col-md-8">
          <div className="card-body" >
            <h5 className="card-title">mr.love</h5>
            <small className="card-text">Loveland Community</small>
            
          </div>
        </div>
      </div>
      </Container>
      <Container>
      <div className="row no-gutters" style={{marginTop:"5px",marginBottom:"5px",backgroundColor :"#FFFFFF" ,borderRadius:"10px"}}>
        <div className="col-md-4 " style={{marginTop:"20px" }}>
          <img
            className="bd-placeholder-img"
            width="50"
            height="50"
            src ={s2}
            
            alt=""
          />
        
        </div>
        <div className="col-md-8">
          <div className="card-body" >
            <h5 className="card-title">Victor_carvel</h5>
            <small className="card-text">Loveland Community</small>
            
          </div>
        </div>
      </div>
      </Container>
      <Container>
      <div className="row no-gutters" style={{marginTop:"5px",marginBottom:"20px",backgroundColor :"#FFFFFF" ,borderRadius:"10px"}}>
        <div className="col-md-4 " style={{marginTop:"20px" }}>
          <img
            className="bd-placeholder-img"
            width="50"
            height="50"
            src ={s3}
            
            alt=""
          />
        
        </div>
        <div className="col-md-8">
          <div className="card-body" >
            <h5 className="card-title">gavin_b7</h5>
            <small className="card-text">Loveland Community</small>
            
          </div>
        </div>
      </div>
      </Container>
      <p>Your current rank is 94</p>
    </div>
  );
}

export default MiniRank;
