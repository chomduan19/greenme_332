import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
// import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
// import FaceIcon from '@material-ui/icons/Face';
// import DoneIcon from '@material-ui/icons/Done';
import water from './img/planttree/watertuning.png'
import o2 from './img/planttree/O2.png'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

function ChipScore({numOxy,numWater}) {
  const classes = useStyles();

  // const handleDelete = () => {
  //   console.info('You clicked the delete icon.');
  // };

  // const handleClick = () => {
  //   console.info('You clicked the Chip.');
  // };

  return (
    <div className={classes.root} height="100px">
      <Chip
        icon={<img alt = "" src= {water} width="30px" />}
        label={numWater}
        clickable
        color="#B1D7A0"
        
        // onDelete={handleDelete}
        // deleteIcon={<DoneIcon />}
      />
      <Chip
        icon={<img alt = "" src= {o2} width="30px" />}
        size ="large"
        label={numOxy}
        clickable
        variant="outlined"
        
        // onDelete={handleDelete}
        // deleteIcon={<DoneIcon />}
      />
    </div>
  );
}
export default ChipScore;
