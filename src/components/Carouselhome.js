import Carousel from 'react-bootstrap/Carousel';
import React,{ Component } from "react";

import img1 from './img/trash.jpg';
import img2 from './img/recycle1.jpg';
import img3 from './img/plant1.jpg';
import img4 from './img/plant2.jpg';
  
export class ControlledCarousel extends Component {  
        render() {  
                return (  
                        
                         <div className='container-fluid' >  
                         <Carousel interval={5000} keyboard={false} pausenonhover={true} borderRadius={"10px"}>  
                         <Carousel.Item style={{'height':"500px"}}  >  
                              <img
                                className="d-block w-100"  
                                alt=""
                                src={img1}  />  
                           <Carousel.Caption>  
                             <h3 className="title__caption" style={{textAlign: "left"}}>Plastic pollution</h3>
                             <p  className="p__caption" style={{textAlign: "left"}}>The problem of plastic in nature, particularly in our oceans, 
                               is a global crisis. Every minute, about a dump-truck load of plastic goes into the oceans, 
                               sullying beaches, hurting wildlife, and contaminating our food supply.</p>  
                            </Carousel.Caption>  
                          </Carousel.Item  >  
                          <Carousel.Item style={{'height':"500px"}}>  
                              <img  
                                className="d-block w-100"  
                                alt=""
                                src={img2}    />  
                            <Carousel.Caption>  
                              <h3 className="title__caption" style={{textAlign: "left"}}>What is GREENME?</h3>  
                              <p  className="p__caption" style={{textAlign: "left"}}>GREENME is the non-profit organization
                              to promoting STOP using plastic. GREENME will collect your plastic waste for recycling it</p>  
                             </Carousel.Caption>  
                          </Carousel.Item>  
                          <Carousel.Item style={{'height':"500px"}}>  
                              <img  
                                 className="d-block w-100"  
                                  alt=""
                                  src={img3}   />  
                               <Carousel.Caption>  
                                   <h3 className="title__caption" style={{textAlign: "left"}}>Plant your tree</h3> 
                                   <p  className="p__caption" style={{textAlign: "left"}}>GREENME's main function is let you plant your tree by point that you collecting from recycle plastic.</p>   
                                </Carousel.Caption>  
                          </Carousel.Item>  
                          <Carousel.Item style={{'height':"500px"}}>  
                               <img 
                                  className="d-block w-100"  
                                  alt=""
                                  src={img4}   />  
                               <Carousel.Caption>  
                                  <h3 className="title__caption" style={{textAlign: "left"}}>Save the world</h3>  
                                  <p  className="p__caption" style={{textAlign: "left"}}>You can make your life and bring your friend's home back just send recycle waste to GREENME. Not only obtain many reward but also save the world.</p>  
                                </Carousel.Caption>  
                          </Carousel.Item>  
                      </Carousel>  
                      </div>
                )  
        }  
}  
  

  export default ControlledCarousel;