import "bootstrap/dist/css/bootstrap.min.css";
import ControlledCarousel from "./components/Carouselhome.js";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import MiniRank from "./components/MiniRank.js";
import HomeMenu from "./components/HomeMenu";

function Home() {
  return (
    <div className="Home">
      
      <Container>
        <Row className="justify-content-md-center">
          <ControlledCarousel></ControlledCarousel>
        </Row>
        <Row>
          <Col sm="4">
            <MiniRank />
          </Col>
          <Col sm="8">
            <HomeMenu />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default Home;