import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
// import Row from "react-bootstrap/Row";
// import Col from "react-bootstrap/Col";
import React, { useState } from "react";
import afteru from "./assets/afteru.png";
import clod from "./assets/clod.jpg";
import mk from "./assets/mk.png";
import on from "./assets/onthetable.jpg";
import qr from "./assets/frame.png";
import Amplify, { API } from "aws-amplify";
// import ControlledCarousel from "./components/Carouselhome.js";


function Reward() {
  const [show, setShow] = useState(false);
  const [data,setData] = useState(null);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  async function getData() {
    const apiName = "GreenmeAPI";
    const path = "/plastic/reward";
    const myInit = {
      headers: { "Content-Type" : "application/json" },
      // body: {
      //   "id": 23456,
      //   "username": "punpun",
      //   "quality": count1 + count2 + count3 + count4 + count5 + count6,
      // },
    };
    try {
      return API.get(apiName, path, myInit);
    } catch (err) {
      console.log(err);
    }
    
  }
  (async function(){
    const data = await getData();
    setData(data)
  })();
  
  
  return (
    <div className="Reward container-fluid container-m" >
      <Container className="ranktable" style={{ marginLeft: '20rem' }}>
       {/* * <Row>
         <Col sm="true"></Col>
          <Col>  */}
            <div
              className="card mb-3"
              style={{
                width: "520px",
                marginTop: "200px",
                borderRadius: "10px",
              }}
            >
              <div className="row no-gutters">
                <div className="col-md-4">
                  <svg
                    className="bd-placeholder-img"
                    width="130%"
                    height="270"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-label="Placeholder: Image"
                    preserveAspectRatio="xMidYMid slice"
                  ></svg>
                  <img
                    alt=""
                    src={afteru}
                    class="rounded float-left"
                    alt="..."
                    style={{ width: "220px", marginTop: "-230px" }}
                  ></img>
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">รับส่วนลด 5% ทุกเมนู</h5>
                    <p className="card-text">
                      {data[0]}
                      ซื้อเมนูใดก็ได้ของ After You รับส่วนลด 5% จากราคาเต็ม
                    </p>
                    <p className="card-text">
                      <small className="text-muted">
                        สถานที่รับสิทธิ์ After You ทุกสาขา
                      </small>
                    </p>
                    <Button
                      variant="primary"
                      onClick={handleShow}
                      style={{
                        background: "#3CB371",
                        border: "#2E8B57",
                        marginTop: "50px",
                      }}
                    >
                      รับสิทธิ์
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>แสดง QRCode นี้ให้กับพนังงาน</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <img alt="" src={qr} className="img-fluid" />
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </div>
                </div>
              </div>
            </div>
          {/* </Col>
          <Col> */}
            <div
              className="card mb-3"
              style={{
                width: "520px",
                // marginTop: "200px",
                borderRadius: "10px",
              }}
            >
              <div className="row no-gutters">
                <div className="col-md-4">
                  <svg
                    className="bd-placeholder-img"
                    width="150%"
                    height="270"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-label="Placeholder: Image"
                    preserveAspectRatio="xMidYMid slice"
                  ></svg>
                  <img
                    alt=""
                    src={on}
                    class="rounded float-left"
                    alt="..."
                    style={{
                      width: "220px",
                      marginTop: "-250px",
                      marginRight: "30px",
                    }}
                  ></img>
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">
                      รับส่วนลด เมนู Balanced Chicken Bowl 40 บาท
                    </h5>
                    <p className="card-text">
                      เมื่อซื้อ On The Table เมนู Balanced Chicken Bowl
                      รับทันทีส่วนลด 40 บาท จากราคาเต็ม 240 บาท
                    </p>
                    <p className="card-text">
                      <small className="text-muted">
                        สถานที่รับสิทธิ์ On The Table ทุกสาขา
                      </small>
                    </p>
                    <Button
                      style={{ background: "#3CB371", border: "#2E8B57" }}
                      variant="primary"
                      onClick={handleShow}
                    >
                      รับสิทธิ์
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>แสกนเพื่อรับสิทธิ์</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <img alt="" src={qr} className="img-fluid" />
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </div>
                </div>
              </div>
            </div>
          {/* </Col>
          <Col> */}
            <div
              className="card mb-3"
              style={{
                width: "520px",
                marginTop: "20px",
                borderRadius: "10px",
              }}
            >
              <div className="row no-gutters">
                <div className="col-md-4">
                  <svg
                    className="bd-placeholder-img"
                    width="130%"
                    height="270"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-label="Placeholder: Image"
                    preserveAspectRatio="xMidYMid slice"
                  ></svg>
                  <img
                    alt=""
                    src={mk}
                    class="rounded float-left"
                    alt="..."
                    style={{ width: "200px", marginTop: "-200px" }}
                  ></img>
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">รับส่วนลด 5% ทุกเมนู</h5>
                    <p className="card-text">
                      ซื้อเมนูใดก็ได้ของ MK Restaurant รับส่วนลด 20% จากราคาเต็ม
                    </p>
                    <p className="card-text">
                      <small className="text-muted">
                        สถานที่รับสิทธิ์ MK Restaurant ทุกสาขา
                      </small>
                    </p>
                    <Button
                      style={{ background: "#3CB371", border: "#2E8B57" }}
                      variant="primary"
                      onClick={handleShow}
                    >
                      รับสิทธิ์
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>แสกนเพื่อรับสิทธิ์</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <img alt="" src={qr} className="img-fluid" />
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </div>
                </div>
              </div>
            </div>
          {/* </Col>
          </Row>
              <Row> */}
          {/* <Col> */}
            <div
              className="card mb-3"
              style={{
                width: "520px",
                marginTop: "20px",
                borderRadius: "10px",
              }}
            >
              <div className="row no-gutters">
                <div className="col-md-4">
                  <svg
                    className="bd-placeholder-img"
                    width="130%"
                    height="270"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-label="Placeholder: Image"
                    preserveAspectRatio="xMidYMid slice"
                  ></svg>
                  <img
                    alt=""
                    src={clod}
                    class="rounded float-left"
                    alt="..."
                    style={{
                      width: "220px",
                      marginTop: "-210px",
                      marginRight: "50px",
                    }}
                  ></img>
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">รับส่วนลด 5% ทุกเมนู</h5>
                    <p className="card-text">
                      ซื้อเมนูใดก็ได้ของ Clod Stone รับส่วนลด 10% จากราคาเต็ม
                    </p>
                    <p className="card-text">
                      <small className="text-muted">
                        สถานที่รับสิทธิ์ Clod Stone ทุกสาขา
                      </small>
                    </p>
                    <Button
                      style={{ background: "#3CB371", border: "#2E8B57" }}
                      variant="primary"
                      onClick={handleShow}
                    >
                      รับสิทธิ์
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>แสกนเพื่อรับสิทธิ์</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <img alt="" src={qr} className="img-fluid" />
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </div>
                </div>
              </div>
            </div>
          {/* </Col>
          <Col></Col>
        </Row> */}
      </Container>
    </div>
  );
}

export default Reward;
